<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentars';

    protected $fillable = [
        'berita_id','nama','email','komentar','balasan',
    ];

    // relasi ke berita
    public function berita()
    {
        return $this->belongsTo('App\Berita', 'berita_id');
    }
}
