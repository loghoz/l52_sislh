<?php

Route::auth();

//USER
Route::resource('/', 'User\WelcomeController@index');
Route::post('/pengaduan/store', 'User\WelcomeController@store');

Route::resource('/profil/visimisi', 'User\ProfilController@visimisi');
Route::resource('/profil/moto', 'User\ProfilController@moto');
Route::resource('/profil/tujuan', 'User\ProfilController@tujuan');
Route::resource('/profil/sasaran', 'User\ProfilController@sasaran');
Route::resource('/profil/struktur', 'User\ProfilController@struktur');

Route::resource('/berita', 'User\BeritaController');
Route::get('cari', 'User\BeritaController@cari');

Route::resource('/layanan/ukl', 'User\LayananController@ukl');
Route::resource('/layanan/lb3', 'User\LayananController@lb3');
Route::resource('/layanan/iplc', 'User\LayananController@iplc');
Route::resource('/layanan/sedotwc', 'User\LayananController@sedotwc');
Route::resource('/layanan/pengelolaansampah', 'User\LayananController@sampah');

Route::resource('/program', 'User\ProgramController@program');
Route::resource('/kebijakan', 'User\ProgramController@kebijakan');

Route::resource('/regulasi/undang', 'User\RegulasiController@undang');
Route::resource('/regulasi/peraturandaerah', 'User\RegulasiController@daerah');

Route::resource('/gallery', 'User\GalleryController');

//ADMIN
Route::resource('/admin/beranda', 'Admin\WelcomController');
Route::resource('/admin/berita', 'Admin\BeritaController');
Route::get('/admin/berita/publish/{id}', 'Admin\BeritaController@publish');
Route::resource('/admin/pengaduan', 'Admin\PengaduanController');
Route::resource('/admin/video', 'Admin\VideoController');
Route::resource('/admin/gallery', 'Admin\AlbumController');
Route::resource('/admin/foto', 'Admin\GalleryController');

Route::resource('/admin/regulasi', 'Admin\RegulasiController');
Route::resource('/admin/profil', 'Admin\ProfilController');
Route::resource('/admin/layanan', 'Admin\LayananController');
Route::resource('/admin/programkebijakan', 'Admin\ProgramController');

Route::resource('/admin/pengaturan', 'Admin\PengaturanController');
Route::resource('/admin/pengaturan/kontak', 'Admin\KontakController');
Route::resource('/admin/info', 'Admin\MetaController');
Route::resource('/admin/banner', 'Admin\BannerController');
Route::resource('/admin/komentar', 'Admin\KomentarController');
