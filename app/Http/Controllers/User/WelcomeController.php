<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pengaduan;
use App\Berita;
use App\Kontak;
use App\Meta;
use App\Video;
class WelcomeController extends Controller
{

    public function index()
    {
        $meta = Meta::get();
        $video = Video::get();
        $kontak = Kontak::where('id',1)->get()->first();
        $berita = Berita::where('publish','T')->orderBy('id','DESC')->paginate(3);

        return view('welcome',compact('berita','kontak','meta','video'));
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $pengaduan = $request->only('nama','email','telp','pesan');

        Pengaduan::create($pengaduan);
        
        $notification = array(
            'message' => 'Pengaduan berhasil dikirim.',
            'alert-type' => 'success'
        );

        return redirect()->route('index')->with($notification);
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
