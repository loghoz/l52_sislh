<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Kontak;
use App\Meta;
class ProfilController extends Controller
{

    public function visimisi()
    {
        $meta = Meta::get();
        $kontak = Kontak::where('id',1)->get()->first();
        return view('user.profil.visimisi',compact('kontak','meta'));
    }
    public function moto()
    {
        $meta = Meta::get();
        $kontak = Kontak::where('id',1)->get()->first();
        return view('user.profil.moto',compact('kontak','meta'));
    }
    public function tujuan()
    {
        $meta = Meta::get();
        $kontak = Kontak::where('id',1)->get()->first();
        return view('user.profil.tujuan',compact('kontak','meta'));
    }
    public function sasaran()
    {
        $meta = Meta::get();
        $kontak = Kontak::where('id',1)->get()->first();
        return view('user.profil.sasaran',compact('kontak','meta'));
    }
    public function struktur()
    {
        $meta = Meta::get();
        $kontak = Kontak::where('id',1)->get()->first();
        return view('user.profil.struktur',compact('kontak','meta'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
