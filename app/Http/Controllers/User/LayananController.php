<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Kontak;

use App\Meta;
class LayananController extends Controller
{

    public function ukl()
    {
        $meta = Meta::get();
        $kontak = Kontak::where('id',1)->get()->first();
        return view('user.layanan.ukl',compact('kontak','meta'));
    }
    public function lb3()
    {
        $meta = Meta::get();
        $kontak = Kontak::where('id',1)->get()->first();
        return view('user.layanan.lb3',compact('kontak','meta'));
    }
    public function iplc()
    {
        $meta = Meta::get();
        $kontak = Kontak::where('id',1)->get()->first();
        return view('user.layanan.iplc',compact('kontak','meta'));
    }
    public function sedotwc()
    {
        $meta = Meta::get();
        $kontak = Kontak::where('id',1)->get()->first();
        return view('user.layanan.sedotwc',compact('kontak','meta'));
    }
    public function sampah()
    {
        $meta = Meta::get();
        $kontak = Kontak::where('id',1)->get()->first();
        return view('user.layanan.sampah',compact('kontak','meta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
