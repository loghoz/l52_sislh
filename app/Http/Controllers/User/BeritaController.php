<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Berita;
use App\Kontak;
use App\Komentar;
class BeritaController extends Controller
{

    public function index()
    {
        $kontak = kontak::where('id',1)->get()->first();
        $berita = Berita::where('publish','T')->orderBy('id','DESC')->paginate(6);

        return view('user.berita.berita',compact('berita','kontak'));
    }

    public function cari(Request $request)
    {
        $kontak = Kontak::where('id',1)->get()->first();
        $cari = $request->get('cari');

        $berita = Berita::Where('isi', 'LIKE', '%'.$cari.'%')
                    ->orderBy('id','DESC')->paginate(6);
        
        return view('user.berita.cari_berita', compact('berita','cari','kontak'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $komentar = $request->only('berita_id','nama','email','komentar');

        Komentar::create($komentar);

        $notification = array(
            'message' => 'Komentar berhasil dikirim.',
            'alert-type' => 'success'
        );

        return redirect('berita/'.$request->get('berita_id'))->with($notification);
    }

    public function show($id)
    {
        $kontak = kontak::where('id',1)->get()->first();
        $berita = Berita::findOrFail($id);
        $beritalain = Berita::where('id', 'NOT LIKE',$id)
                ->orderBy('id','DESC')
                ->paginate(5);

        $komentar = Komentar::where('berita_id', $id)
                ->orderBy('id','DESC')
                ->paginate(10);

        return view('user.berita.show_berita', compact('berita','kontak','beritalain','komentar'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
