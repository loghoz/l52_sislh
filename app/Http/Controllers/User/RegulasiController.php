<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Kontak;
use App\Meta;
class RegulasiController extends Controller
{

    public function undang()
    {
        $kontak = Kontak::where('id',1)->get()->first();
        $regulasi = Meta::where('meta_key','Regulasi - Undang-undang')->get()->all();
        return view('user.regulasi.undang',compact('kontak','regulasi'));
    }

    public function daerah()
    {
        $kontak = Kontak::where('id',1)->get()->first();
        $regulasi = Meta::where('meta_key','Regulasi - Peraturan Daerah')->get()->all();
        return view('user.regulasi.daerah',compact('kontak','regulasi'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
