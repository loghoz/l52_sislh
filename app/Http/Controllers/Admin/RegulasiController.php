<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Meta;

class RegulasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $no=1;
        $regulasi = Meta::where('meta_key','LIKE', '%Regulasi%')
                    ->get()->all();

        return view('admin.regulasi.regulasi', compact('regulasi','no'));
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $regulasi = Meta::findOrFail($id);

        return view('admin.regulasi.show_regulasi', compact('regulasi'));
    }

    public function edit($id)
    {
        $regulasi = Meta::findOrFail($id);
        return view('admin.regulasi.edit_regulasi', compact('regulasi'));
    }

    public function update(Request $request, $id)
    {
        $regulasi = Meta::findOrFail($id);

        if ($request->hasFile('meta_value')) {
            $this->deletePhoto($regulasi->photo);
            $dataregulasi['meta_value'] = $this->savePhoto($request->file('meta_value'));
        }

        $regulasi->update($dataregulasi);

        $notification = array(
            'message' => 'Regulasi berhasil diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.regulasi.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = str_random(40) . '.' . $photo->guessClientExtension();
      $destinationPath = 'itlabil/file';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'itlabil/file/'.$filename;
      return File::delete($path);
    }
}
