<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\AlbumFoto;
use App\Gallery;

class AlbumController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $album = AlbumFoto::orderBy('id','DESC')->paginate(8);

        return view('admin.album.album', compact('album'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        // SIMPAN DATA SISWA META
        $album = $request->only('album');

        AlbumFoto::create($album);

        $notification = array(
            'message' => 'Album Baru berhasil dibuat.',
            'alert-type' => 'succes'
        );

        return redirect()->route('admin.gallery.index')->with($notification);
    }

    public function show($id)
    {
        $album = AlbumFoto::findOrFail($id);
        $foto = Gallery::where('album_id',$album->id)->orderBy('id','DESC')->paginate(8);

        return view('admin.album.show_album', compact('album','foto'));
    }


    public function edit($id)
    {
        $album = AlbumFoto::findOrFail($id);
        return view('admin.album.edit_album', compact('album'));
    }

    public function update(Request $request, $id)
    {
        $album = AlbumFoto::findOrFail($id);

        $dataalbum = $request->all();
        $album->update($dataalbum);

        $notification = array(
            'message' => 'Album berhasil diubah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.gallery.index')->with($notification);
    }

    public function destroy($id)
    {
        $album = AlbumFoto::findOrFail($id);

        $hapus = Gallery::where('album_id',$album->id)->get()->all();
        foreach ($hapus as $item) { 
            $this->deletePhoto($item->photo);
            Gallery::find($item->id)->delete();
        }

        $notification = array(
            'message' => 'Album '.$album->album.' berhasil dihapus.',
            'alert-type' => 'error'
        );

        AlbumFoto::find($id)->delete();

        return redirect()->route('admin.gallery.index')->with($notification);
    }

    public function deletePhoto($filename)
    {
      $path = 'itlabil/images/gallery/'.$filename;
      return File::delete($path);
    }
}
