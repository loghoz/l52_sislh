<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\kontak;
class PengaturanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $user = User::where('name','Admin')->get();
        $kontak = kontak::get()->first();
        return view('admin.pengaturan',compact('user','kontak'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $akun = User::select('id','id')->findOrFail($id);

        return view('admin.pengaturan.edit_admin', compact('akun'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'required'
        ], [
            'password.required' => 'Password harus diisi!'
        ]);

        $akun = User::findOrFail($id);

        $password = $request->get('password');

        if ($password == '') {
        } else {
            $data['password'] = bcrypt($request->get('password'));
        }

        $akun->update($data);

        $notification = array(
            'message' => 'Password berhasil diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.pengaturan.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }
}
