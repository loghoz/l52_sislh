<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Meta;
class MetaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {

        $info = Meta::OrderBy('meta_key','ASC')->get();

        return view('admin.info.info', compact('info'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Meta::create($request->all());
        $notification = array(
            'message' => 'Informasi berhasil ditambah.',
            'alert-type' => 'succes'
        );
        return redirect()->route('admin.info.index')->with($notification);
    }

    public function show($id)
    {
        $info = Meta::findOrFail($id);

        return view('admin.info.show_info', compact('info'));
    }

    public function edit($id)
    {
        $info = Meta::findOrFail($id);

        return view('admin.info.edit_info', compact('info'));
    }

    public function update(Request $request, $id)
    {
        $info = Meta::findOrFail($id);

        $info->update($request->all());
        $notification = array(
            'message' => 'Informasi berhasil diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.info.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }
}
