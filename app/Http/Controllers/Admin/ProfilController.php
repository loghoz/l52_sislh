<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Meta;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {

        $data = Meta::where('meta_key','LIKE', '%Profil%')->OrderBy('meta_key','ASC')->get();

        return view('admin.profil.profil', compact('data'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $data = Meta::findOrFail($id);

        return view('admin.profil.show_profil', compact('data'));
    }

    public function edit($id)
    {
        $data = Meta::findOrFail($id);

        return view('admin.profil.edit_profil', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Meta::findOrFail($id);

        $data->update($request->all());
        $notification = array(
            'message' => 'Data '.$data->meta_key.' berhasil diubah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.profil.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }
}
