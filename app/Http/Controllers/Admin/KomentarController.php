<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Berita;
use App\Komentar;
class KomentarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }

        $komentar = Komentar::orderBy('id','DESC')->paginate(10);

        return view('admin.komentar.komentar', compact('komentar','no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $komentar = Komentar::findOrFail($id);
        return view('admin.komentar.edit_komentar', compact('komentar'));
    }

    public function update(Request $request, $id)
    {
        $komentar= Komentar::findOrFail($id);

        if ($komentar->balasan!='') {
            $notification = array(
                'message' => 'Balasan Komentar telah diubah.',
                'alert-type' => 'success'
            );
        } else {
            $notification = array(
                'message' => 'Komentar telah dibalas.',
                'alert-type' => 'info'
            );
        }
        
        $data = $request->only('balasan');
        $komentar->update($data);

        return redirect()->route('admin.komentar.index')->with($notification);
    }

    public function destroy($id)
    {  
        $notification = array(
            'message' => 'Komentar berhasil dihapus.',
            'alert-type' => 'error'
        );
        
        Komentar::find($id)->delete();
        return redirect()->route('admin.komentar.index')->with($notification);
    }
}
