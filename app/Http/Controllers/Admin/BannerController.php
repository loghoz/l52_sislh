<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Banner;

class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {

        $banner = Banner::get();

        return view('admin.banner.banner', compact('banner'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        return view('admin.banner.edit_banner', compact('banner'));
    }

    public function update(Request $request, $id)
    {
        $banner = Banner::findOrFail($id);

        $databanner = $request->only('judul');

        if ($request->hasFile('photo')) {
            $this->deletePhoto($banner->photo);
            $databanner['photo'] = $this->savePhoto($request->file('photo'));
        }else{
            $databanner['photo'] = $banner->photo;
        }

        $banner->update($databanner);

        $notification = array(
            'message' => 'Banner berhasil diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.banner.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = $photo->getClientOriginalName();
      $destinationPath = 'itlabil/images/default';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'itlabil/images/default/'.$filename;
      return File::delete($path);
    }
}
