<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Video;

class VideoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $data = Video::all();

        return view('admin.video.video', compact('data'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Video::findOrFail($id);
        return view('admin.video.edit_video', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Video::findOrFail($id);

        $datavideo = $request->all();
        $data->update($datavideo);

        $notification = array(
            'message' => 'Video berhasil diganti.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.video.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }
}
