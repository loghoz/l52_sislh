<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Berita;

class BeritaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function publish(Request $request,$id)
    {
        $berita = Berita::findOrFail($id);

        if ($berita->publish =='T') {
            $databerita['publish'] = 'F';
        } else {
            $databerita['publish'] = 'T';
        }
        
        $berita->update($databerita);

        if ($berita->publish =='F') {
            $notification = array(
                'message' => 'Berita Tidak Aktif.',
                'alert-type' => 'info'
            );
        } else {
            $notification = array(
                'message' => 'Berita Aktif.',
                'alert-type' => 'info'
            );
        }
        
        return redirect()->route('admin.berita.index')->with($notification);
    }

    public function index(Request $request)
    {
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }

        $berita = Berita::orderBy('id','DESC')->paginate(10);

        return view('admin.berita.berita', compact('berita','no'));
    }

    public function create()
    {
        return view('admin.berita.tambahberita');
    }

    public function store(Request $request)
    {
        // SIMPAN DATA SISWA META
        $berita = $request->only('judul','isi');

        if ($request->hasFile('photo')) {
            $berita['photo'] = $this->savePhoto($request->file('photo'));
        } else {
            $berita['photo'] = '-';
        }

        $berita['publish'] = 'F';
        
        Berita::create($berita);

        $notification = array(
            'message' => 'Berita berhasil ditambah.',
            'alert-type' => 'succes'
        );
        return redirect()->route('admin.berita.index')->with($notification);
    }

    public function show($id)
    {
        $berita = Berita::findOrFail($id);

        return view('admin.berita.show_berita', compact('berita'));
    }

    public function edit($id)
    {
        $berita = Berita::findOrFail($id);
        return view('admin.berita.edit_berita', compact('berita'));
    }

    public function update(Request $request, $id)
    {
        $berita = Berita::findOrFail($id);

        $databerita = $request->only('judul','isi');

        if ($request->hasFile('photo')) {
            $this->deletePhoto($berita->photo);
            $databerita['photo'] = $this->savePhoto($request->file('photo'));
        }

        $berita->update($databerita);
        $notification = array(
            'message' => 'Berita berhasil diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.berita.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = Berita::findOrFail($id);

        $berita = Berita::find($id);
        $this->deletePhoto($berita->photo);
  
        $notification = array(
            'message' => 'Berita berhasil dihapus.',
            'alert-type' => 'error'
        );
        
        Berita::find($id)->delete();
        return redirect()->route('admin.berita.index')->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = str_random(40) . '.' . $photo->guessClientExtension();
      $destinationPath = 'itlabil/images/berita';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'itlabil/images/berita/'.$filename;
      return File::delete($path);
    }
}
