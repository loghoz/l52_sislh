<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Gallery;

class GalleryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $gallery = Gallery::orderBy('id','DESC')->paginate(8);

        return view('admin.gallery.gallery', compact('gallery'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required',
            'photo.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);


        if ($image = $request->file('photo')) {
            foreach ($image as $files) {
                $id=$request->album_id;
                $name = str_random(40) . '.' . $files->guessClientExtension();
                $destinationPath = 'itlabil/images/gallery';
                $files->move($destinationPath, $name);

                $insert[]  = [
                    'photo'     => $name,
                    'album_id'  =>  $id
                ];
            }
        }
       
        Gallery::insert($insert);

        $notification = array(
            'message' => 'Gambar berhasil ditambah.',
            'alert-type' => 'succes'
        );

        return redirect()->route('admin.gallery.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $hapus = Gallery::findOrFail($id);

        $gallery = Gallery::find($id);
        $this->deletePhoto($gallery->photo);
  
        $notification = array(
            'message' => 'Gambar berhasil dihapus.',
            'alert-type' => 'error'
        );

        Gallery::find($id)->delete();

        return back()->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = str_random(40) . '.' . $photo->guessClientExtension();
      $destinationPath = 'itlabil/images/gallery';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'itlabil/images/gallery/'.$filename;
      return File::delete($path);
    }
}
