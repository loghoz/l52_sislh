<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'galleries';

    protected $fillable = [
        'album_id','photo',
    ];

    // relasi ke album
    public function album()
    {
        return $this->belongsTo('App\Album', 'album_id');
    }
}
