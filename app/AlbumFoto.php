<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumFoto extends Model
{
    protected $table = 'album_fotos';

    protected $fillable = [
        'album'
    ];

    public function gallery()
    {
        return $this->hasMany('App\Gallery','album_id');
    }
}
