<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomentarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('berita_id')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->text('komentar');
            $table->text('balasan');
            $table->timestamps();

            $table->foreign('berita_id')
                ->references('id')
                ->on('beritas')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('komentars');
    }
}
