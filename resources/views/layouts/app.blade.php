<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="{{ asset ('itlabil/images/default/logo-pesawaran.png') }}">
        <title>Admin SIS-LH</title>

        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
       
        <!-- CSS -->

        <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/morris.js/morris.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <link href="{{ asset('itlabil/user/toast/toastr.min.css') }}" rel="stylesheet">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <!-- HEADER -->
            <header class="main-header">
                <a href="{{ asset('admin/beranda') }}" class="logo">
                    <span class="logo-mini"><b>SI</b>LH</span>
                    <span class="logo-lg"><b>SIS-</b>LH</span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                        
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{ asset('itlabil/admin/dist/img/avatar5.png') }}" class="user-image" alt="User Image">
                                    <span class="hidden-xs">Admin</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="{{ asset('itlabil/admin/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
                                        <p>ADMIN SISLH</p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div align="center">
                                            <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Keluar</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                            <!-- Control Sidebar Toggle Button -->
                            <!-- <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li> -->
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- MENU SIDE BAR -->
            <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu" data-widget="tree">
                        <li><a href="{{ asset('admin/beranda') }}"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i>
                                <span>Berita</span>
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ asset('admin/berita') }}"><i class="fa fa-circle-o"></i> Data Berita</a></li>
                                <li><a href="{{ asset('admin/berita/create') }}"><i class="fa fa-circle-o"></i> Tambah Berita</a></li>
                                <li><a href="{{ asset('admin/komentar') }}"><i class="fa fa-circle-o"></i></i> Komentar</a></li>
                            </ul>
                        </li>
                        
                        <li><a href="{{ asset('admin/profil') }}"><i class="fa fa-child"></i> <span>Profil</span></a></li>
                        <!-- <li><a href="{{ asset('admin/layanan') }}"><i class="fa fa-hand-o-right"></i> <span>Layanan</span></a></li> -->
                        <li><a href="{{ asset('admin/programkebijakan') }}"><i class="fa fa-retweet"></i> <span>Program & Kebijakan</span></a></li>
                        <li><a href="{{ asset('admin/regulasi') }}"><i class="fa fa-bank"></i> <span>Regulasi</span></a></li>
                        <li><a href="{{ asset('admin/pengaduan') }}"><i class="fa fa-commenting"></i> <span>Pengaduan</span></a></li>
                        <!-- <li><a href="{{ asset('admin/gallery') }}"><i class="fa fa-photo"></i> <span>Gallery</span></a></li> -->

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-photo"></i>
                                <span>Gallery</span>
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ asset('admin/gallery') }}"><i class="fa fa-circle-o"></i> Foto</a></li>
                                <li><a href="{{ asset('admin/video') }}"><i class="fa fa-circle-o"></i> Video</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-cog"></i>
                                <span>Pengaturan</span>
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ asset('admin/pengaturan') }}"><i class="fa fa-circle-o"></i> Akun</a></li>
                                <!-- <li><a href="{{ asset('admin/info') }}"><i class="fa fa-circle-o"></i> Informasi</a></li> -->
                                <li><a href="{{ asset('admin/banner') }}"><i class="fa fa-circle-o"></i> Banner</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
            </aside>

            <!-- CONTENT -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    @yield('content')
                </section>
            </div>

            <!-- FOOTER -->
            <footer class="main-footer">
                <strong>Copyright &copy; 2018 <a href="#">Sistem Informasi Lingkungan Hidup</a>
            </footer>

            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- JS -->
        <script src="{{ asset('itlabil/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <script src="{{ asset('itlabil/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/raphael/raphael.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/morris.js/morris.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
        <script src="{{ asset('itlabil/admin/dist/js/adminlte.min.js') }}"></script>
        <script src="{{ asset('itlabil/admin/dist/js/pages/dashboard.js') }}"></script>
        <script src="{{ asset('itlabil/admin/dist/js/demo.js') }}"></script>
                
        <script type="text/javascript" src="{{ asset('itlabil/user/toast/toastr.min.js') }}"></script>

        <script>
            @if(Session::has('message'))
                var type="{{Session::get('alert-type','info')}}"

                switch(type){
                    case 'info':
                        toastr.info("{{ Session::get('message') }}");
                        break;
                    case 'success':
                        toastr.success("{{ Session::get('message') }}");
                        break;
                    case 'warning':
                        toastr.warning("{{ Session::get('message') }}");
                        break;
                    case 'error':
                        toastr.error("{{ Session::get('message') }}");
                        break;
                }
            @endif
        </script>
    </body>
</html>
