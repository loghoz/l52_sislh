<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="{{ asset ('itlabil/images/default/logo-pesawaran.png') }}">
        <title>Dinas Lingkungan Hidup</title>
        
        <style>
            .slider .item-first {
                background-image: url({{ url('itlabil/images/default/slider1.jpg')}});
            }

            .slider .item-second {
                background-image: url({{ url('itlabil/images/default/slider2.jpg')}});
            }

            .slider .item-third {
                background-image: url({{ url('itlabil/images/default/slider3.jpg')}});
            }
        </style>

        <!-- Styles -->
        <link href="{{ asset('itlabil/user/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/user/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/user/css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/user/css/owl.carousel.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/user/css/owl.theme.default.min.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/user/css/itlabil-style.css') }}" rel="stylesheet">
        <link href="{{ asset('itlabil/user/css/gallery-grid.css') }}" rel="stylesheet"> 
        <link href="{{ asset('itlabil/user/css/baguetteBox.min.css') }}" rel="stylesheet"> 
        <!-- <link href="{{ asset('itlabil/user/css/tooplate-style.css') }}" rel="stylesheet">  -->

        <link href="{{ asset('itlabil/user/toast/toastr.min.css') }}" rel="stylesheet">
    </head>

    <body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

        <!-- PRE LOADER FIX-->
        <section class="preloader">
            <div class="spinner">

                <span class="spinner-rotate"></span>
                
            </div>
        </section>

        <!-- HEADER FIX -->
        <header>
            <div class="container">
                <div class="row">

                        <div class="col-md-4 col-sm-5">
                            <p>Selamat Datang di Website Dinas Lingkungan Hidup</p>
                        </div>
                            
                        <div class="col-md-8 col-sm-7 text-align-right">
                            <span class="phone-icon"><i class="fa fa-phone"></i><a href="tel:{{$kontak->telp}}"> {{$kontak->telp}}</a></span>
                            <span class="date-icon"><i class="fa fa-calendar-plus-o"></i> 08:00 - 15:00 WIB (Sen-Kam)</span>
                            <span class="email-icon"><i class="fa fa-calendar-plus-o"></i> 08:30 - 14.30 WIB (Jum)</span>
                        </div>

                </div>
            </div>
        </header>

        <!-- MENU FIX-->
        <section class="navbar navbar-default navbar-static-top" role="navigation">
            <div class="container">

                <div class="navbar-header">
                        <button class="navbar-toggle" data-toggle="modal" data-target="#modal-menu">
                            <span class="icon icon-bar"></span>
                            <span class="icon icon-bar"></span>
                            <span class="icon icon-bar"></span>
                        </button>

                        <a href="{{ asset('/') }}" class="navbar-brand">DLH</a>
                </div>

                <!-- MENU LINKS -->
                <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#top" class="smoothScroll">Beranda</a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Profil
                                </a>
                                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownPortfolio">
                                    <a class="dropdown-item" href="{{ asset('profil/visimisi') }}">Visi & Misi</a>
                                    <a class="dropdown-item" href="{{ asset('profil/moto') }}">Moto</a>
                                    <a class="dropdown-item" href="{{ asset('profil/tujuan') }}">Tujuan</a>
                                    <a class="dropdown-item" href="{{ asset('profil/sasaran') }}">Sasaran</a>
                                    <a class="dropdown-item" href="{{ asset('profil/struktur') }}">Struktur Organisasi</a>
                                </div>
                            </li>
                            <li><a href="{{ asset('berita') }}" class="smoothScroll">Berita</a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Layanan
                                </a>
                                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownPortfolio">
                                    <a class="dropdown-item" href="{{ asset('layanan/ukl') }}">UKL-UPL/DPLH</a>
                                    <a class="dropdown-item" href="{{ asset('layanan/lb3') }}">LB3</a>
                                    <a class="dropdown-item" href="{{ asset('layanan/iplc') }}">IPLC</a>
                                    <a class="dropdown-item" href="{{ asset('layanan/sedotwc') }}">Sedot WC</a>
                                    <a class="dropdown-item" href="{{ asset('layanan/pengelolaansampah') }}">Pengelolaan Sampah</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Program & Kebijakan
                                </a>
                                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownPortfolio">
                                    <a class="dropdown-item" href="{{ asset('program') }}">Program</a>
                                    <a class="dropdown-item" href="{{ asset('kebijakan') }}">Kebijakan</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Regulasi
                                </a>
                                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownPortfolio">
                                    <a class="dropdown-item" href="{{ asset('regulasi/undang') }}">Undang - Undang</a>
                                    <a class="dropdown-item" href="{{ asset('regulasi/peraturandaerah') }}">Peraturan Daerah</a>
                                </div>
                            </li>
                            <li><a href="{{ asset('gallery') }}">Gallery</a></li>
                            <li class="appointment-btn"><a href="#appointment">Kotak Pengaduan</a></li>
                            <li class="appointment-btn"><a data-toggle="modal" data-target="#modal-default"><i class="fa fa-search"></i></a></li>
                        </ul>
                </div>

            </div>
        </section>

        <!-- Banner Fix -->
        <section id="home" class="slider" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row">

                    <div id="owl-demo" class="owl-carousel owl-theme">
    
                        <div class="item"><img src="{{ url('itlabil/images/default/slider1.jpg')}}" alt="Slider 1"></div>
                        <div class="item"><img src="{{ url('itlabil/images/default/slider2.jpg')}}" alt="Slider 2"></div>
                        <div class="item"><img src="{{ url('itlabil/images/default/slider3.jpg')}}" alt="Slider 3"></div>

                    </div>

                </div>
            </div>
        </section>

        <!-- SAMBUTAN FIX -->
        <section id="sambutan">
            <div class="container">
                <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="about-info">
                                <h3 class="wow fadeInUp" data-wow-delay="0.6s">Selamat Datang di Website Sistem Informasi Dinas Pengelolaan Lingkungan Hidup</h3>
                                <br><div class="wow fadeInUp" data-wow-delay="0.8s">
                                    @foreach($meta as $item)
                                        @if($item->meta_key=='Kata Sambutan')
                                            {!! $item->meta_value !!}
                                        @endif
                                    @endforeach
                                </div>
                                <figure class="profile wow fadeInUp" data-wow-delay="1s">
                                    <img src="itlabil/images/default/author-image.jpg" class="img-responsive" alt="">
                                    <figcaption>
                                            <h3>
                                                @foreach($meta as $item)
                                                    @if($item->meta_key=='Nama Kepala Dinas')
                                                        {!! $item->meta_value !!}
                                                    @endif
                                                @endforeach
                                            </h3>
                                            <p>Kepala Dinas Lingkungan Hidup</p>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                </div>
            </div>
        </section>

        <!-- BERITA FIX -->
        <section id="news" data-stellar-background-ratio="2.5">
            <div class="container">
                <div class="row">
    
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                                <h2 class="news-title">Berita Terbaru</h2>
                            </div>
                        </div>

                        @foreach($berita as $item)
                        <div class="col-md-4 col-sm-6">
                            <div class="news-thumb wow fadeInUp" data-wow-delay="0.4s">
                                    <a href="{{ route('berita.show', $item->id) }}">
                                        @if( $item->photo == "-")
                                            <img class="img-berita img-responsive"  src="itlabil/images/default/default-berita.png" alt="">
                                        @else	
                                            <img src="itlabil/images/berita/{{ $item->photo }}" class="img-berita sponsive" alt="">	
                                        @endif		  
                                    </a>
                                    <div class="news-info">
                                        <span>{{ $item->created_at->format('d M Y') }}</span>
                                        <h3><a href="{{ route('berita.show', $item->id) }}" title="{{ $item->judul }}">{{ str_limit($item->judul, 30) }}</a></h3>
                                        <p>{!! str_limit($item->isi, 100) !!}</p>
                                    </div>
                            </div>
                            <br>
                        </div>

                        @endforeach
                    </div>
                </div>
        </section>

        <!-- LAYANAN FIX -->
        <section id="layanan" data-stellar-background-ratio="1">
            <div class="container">
                <div class="row">

                        <div class="col-md-12 col-sm-12">
                            <div class="layanan-title">
                                <h2 class="wow fadeInUp" data-wow-delay="0.1s">Layanan</h2>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-1"></div>

                        <a href="/layanan/ukl">
                            <div class="col-md-2 col-sm-4" align="center">
                                <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">
                                    <img src="itlabil/images/default/lay_1.png" class="img-responsive" alt="">

                                        <!-- <div class="layanan-info">
                                                <h5>UKL-UPL/DPLH</h5>
                                        </div> -->
                                </div>
                            </div>
                        </a>

                        <a href="/layanan/lb3">
                            <div class="col-md-2 col-sm-4" align="center">
                                <div class="team-thumb wow fadeInUp" data-wow-delay="0.4s">
                                    <img src="itlabil/images/default/lay_2.png" class="img-responsive" alt="">

                                        <!-- <div class="layanan-info">
                                                <h5>LB3</h5>
                                        </div> -->

                                </div>
                            </div>
                        </a>

                        <a href="/layanan/iplc">
                            <div class="col-md-2 col-sm-4" align="center">
                                <div class="team-thumb wow fadeInUp" data-wow-delay="0.6s">
                                    <img src="itlabil/images/default/lay_3.png" class="img-responsive" alt="">

                                        <!-- <div class="layanan-info">
                                                <h5>IPLC</h5>
                                        </div> -->

                                </div>
                            </div>
                        </a>

                        <a href="/layanan/sedotwc">
                            <div class="col-md-2 col-sm-4" align="center">
                                    <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">
                                        <img src="itlabil/images/default/lay_4.png" class="img-responsive" alt="">
        
                                            <!-- <div class="layanan-info">
                                                <h5>Sedot WC</h5>
                                            </div> -->
        
                                    </div>
                            </div>
                        </a>
    
                        <a href="/layanan/pengelolaansampah">
                            <div class="col-md-2 col-sm-4" align="center">
                                    <div class="team-thumb wow fadeInUp" data-wow-delay="0.4s">
                                        <img src="itlabil/images/default/lay_5.png" class="img-responsive" alt="">
        
                                            <!-- <div class="layanan-info">
                                                <h5>Pengelolaan Sampah</h5>
                                            </div> -->
        
                                    </div>
                            </div>
                        </a>
                        <div class="col-md-1"></div>
                </div>
            </div>
        </section>

        <!-- PENGADUAN FIX -->
        <section id="appointment" data-stellar-background-ratio="3">
            <div class="container">
                <div class="row">

                        <div class="col-md-6 col-sm-6 video-center">
                            <!-- <img src="itlabil/images/default/appointment-image.jpg" class="img-responsive" alt=""> -->
                            <div class="col-md-10">
                                @foreach($video as $vid)
                                    {!!$vid->video!!}
                                @endforeach
                            </div>
                            <div class="col-md-2"></div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <!-- CONTACT FORM HERE -->
                            <form id="appointment-form" role="form" method="post" action="/pengaduan/store">
                            {{ csrf_field() }}
                                <!-- SECTION TITLE -->
                                <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                    <h2 class="news-title">Kotak Pengaduan</h2>
                                </div>

                                <div class="wow fadeInUp" data-wow-delay="0.8s">
                                    <div class="col-md-6 col-sm-6">
                                            <label for="name">Nama</label>
                                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap">
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                    </div>

                                    <div class="col-md-12 col-sm-12">
                                            <label for="telephone">No. Telp / HP</label>
                                            <input type="tel" class="form-control" id="telp" name="telp" placeholder="No. Telp / HP">
                                            <label for="Message">Pesan</label>
                                            <textarea class="form-control" rows="5" id="pesan" name="pesan" placeholder="Tulis Pesan"></textarea>
                                            <button type="submit" class="form-control" id="cf-submit" name="submit">Kirim Pengaduan</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                </div>
            </div>
        </section>

        <!-- GOOGLE MAP FIX -->
        <section id="google-map">
            <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyD4uaw4npeG0gnALXAQs86oVmBYZ800kQY&q=place_id:ChIJi1Idi2zSQC4RzGUVWZUq4EQ" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
        </section> 

        <!-- FOOTER FIX-->
        <footer data-stellar-background-ratio="5">
            <div class="container">
                <div class="row">

                        <div class="col-md-4 col-sm-4">
                            <div class="footer-thumb"> 
                                <h4 class="wow fadeInUp" data-wow-delay="0.4s">Info Kontak</h4>
                                <div class="contact-info">
                                    <p class="footer-text"><i class="fa fa-phone white"></i><a href="tel:{{$kontak->telp}}"> {{$kontak->telp}}</a></p>
                                    <p class="footer-text"><i class="fa fa-envelope-o"></i><a href="mailto:{{$kontak->email}}"> {{$kontak->email}}</a></p>
                                    <p class="footer-text"><i class="fa fa-home"></i> {{$kontak->alamat}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4"> 
                            <div class="footer-thumb">
                                <div class="opening-hours">
                                    <h4 class="wow fadeInUp" data-wow-delay="0.4s">Link Terkait</h4>
                                    <p><a href="http://www.pesawarankab.go.id" target="_blank">Portal Resmi Kabupaten Pesawaran</a></p>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4"> 
                            <div class="footer-thumb">
                                <div class="opening-hours">
                                    <h4 class="wow fadeInUp" data-wow-delay="0.4s">Jam Kerja</h4>
                                    <p class="footer-text">Senin - Kamis <span>08:00 - 15:00 WIB</span></p>
                                    <p class="footer-text">Jum'at <span>08:30 - 14:30 WIB</span></p>
                                </div> 

                                <ul class="social-icon">
                                    <li><a href="{{$kontak->facebook}}" target="_blank" class="fa fa-facebook-square" attr="facebook icon"></a></li>
                                    <li><a href="{{$kontak->twitter}}" target="_blank" class="fa fa-twitter" attr="facebook icon"></a></li>
                                    <li><a href="{{$kontak->instagram}}" target="_blank" class="fa fa-instagram"></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 border-top">
                            <div class="col-md-10 col-sm-10">
                                <div class="copyright-text"> 
                                    <p>Copyright &copy; 2018 SIS-LH</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 text-align-center">
                                <div class="angle-up-btn"> 
                                    <a href="#top" class="smoothScroll wow fadeInUp" data-wow-delay="1.2s"><i class="fa fa-angle-up"></i></a>
                                </div>
                            </div>   
                        </div>
                        
                </div>
            </div>
        </footer>
                
        <div class="modal fade" id="modal-menu">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Menu</h4>
                    </div>
                    <div class="modal-body">
                        <a href="#top" class="smoothScroll">Beranda</a>
                        <hr>
                        <a href="{{ asset('profil/visimisi') }}">Visi & Misi</a><br>
                        <a href="{{ asset('profil/moto') }}">Moto</a><br>
                        <a href="{{ asset('profil/tujuan') }}">Tujuan</a><br>
                        <a href="{{ asset('profil/sasaran') }}">Sasaran</a><br>
                        <a href="{{ asset('profil/struktur') }}">Struktur Organisasi</a>
                        <hr>
                        <a href="{{ asset('berita') }}" class="smoothScroll">Berita</a>
                        <hr>
                        <a href="{{ asset('layanan/ukl') }}">UKL-UPL/DPLH</a><br>
                        <a href="{{ asset('layanan/lb3') }}">LB3</a><br>
                        <a href="{{ asset('layanan/iplc') }}">IPLC</a><br>
                        <a href="{{ asset('layanan/sedotwc') }}">Sedot WC</a><br>
                        <a href="{{ asset('layanan/pengelolaansampah') }}">Pengelolaan Sampah</a>
                        <hr>
                        <a href="{{ asset('program') }}">Program</a><br>
                        <a href="{{ asset('kebijakan') }}">Kebijakan</a>
                        <hr>
                        <a href="{{ asset('regulasi/undang') }}">Undang - Undang</a><br>
                        <a href="{{ asset('regulasi/peraturandaerah') }}">Peraturan Daerah</a>
                        <hr>
                        <a href="{{ asset('gallery') }}">Gallery</a>
                        <hr>
                        <a href="#appointment">Kotak Pengaduan</a>
                        <hr>
                        <a data-toggle="modal" data-target="#modal-default"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal-->
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="GET" action="{{ url('cari') }}">
                        <div class="modal-body">
                            <input type="text" class="form-control" name="cari" placeholder="Cari...">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-success">Cari</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- SCRIPTS -->
        <script src="{{ asset('itlabil/user/js/jquery.js') }}"></script>
        <script src="{{ asset('itlabil/user/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/js/jquery.sticky.js') }}"></script>
        <script src="{{ asset('itlabil/user/js/jquery.stellar.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/js/wow.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/js/smoothscroll.js') }}"></script>
        <script src="{{ asset('itlabil/user/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/js/custom.js') }}"></script>
        <script src="{{ asset('itlabil/user/js/baguetteBox.min.js') }}"></script>
        <script>
            baguetteBox.run('.tz-gallery');
        </script>

        <script type="text/javascript" src="{{ asset('itlabil/user/toast/toastr.min.js') }}"></script>

        <script>
            @if(Session::has('message'))
                var type="{{Session::get('alert-type','info')}}"

                switch(type){
                    case 'info':
                        toastr.info("{{ Session::get('message') }}");
                        break;
                    case 'success':
                        toastr.success("{{ Session::get('message') }}");
                        break;
                    case 'warning':
                        toastr.warning("{{ Session::get('message') }}");
                        break;
                    case 'error':
                        toastr.error("{{ Session::get('message') }}");
                        break;
                }
            @endif
        </script>
    </body>

</html>
