@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Pengaduan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Pengaduan</h3>
                    </div>
                    <div class="box-body">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Nama</td>
                                <td>Email</td>
                                <td>No Telp</td>
                                <td>Pesan</td>
                                <td>Tanggal</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($pengaduan as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->nama }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->telp }}</td>
                                    <td>{{ $item->pesan }}</td>
                                    <td>
                                        <span class="label label-success">{{ $item->created_at->format('h:m') }}</span>
                                        <span class="label label-info">{{ $item->created_at->format('d M Y') }}</span>
                                    </td>
                                    <td>
                                        {!! Form::model($item, ['route' => ['admin.pengaduan.destroy', $item], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                            {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                        {!! Form::close()!!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $pengaduan->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
