@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Video
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Video</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($data, ['route' => ['admin.video.update', $data],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_video', ['model' => $data])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
