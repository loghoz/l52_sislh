@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Video
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Video</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            @foreach($data as $item)
                                {!! $item->video !!}
                                
                                <a href="{{ route('admin.video.edit', $item->id) }}" class="btn btn-success">Ubah Video</a>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
