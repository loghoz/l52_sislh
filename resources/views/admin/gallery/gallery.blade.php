@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Gallery
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Gambar</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.gallery.store', 'class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_gallery')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="col-md-12">
                    @foreach($gallery as $item)
                        <div class="col-md-3">
                            <img src="../itlabil/images/gallery/{{ $item->photo }}" alt="{{ $item->photo }}" class="img-thumbnail">
                            <br>
                            <center>
                            {!! Form::model($item, ['route' => ['admin.gallery.destroy', $item], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                            {!! Form::close()!!}
                            </center><br><br>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-12" align="center">
                    {{ $gallery->appends(compact('page'))->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection
