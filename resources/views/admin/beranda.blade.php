@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">

                <div align="center"><br><br>
                    <img src="../itlabil/images/default/logo-pesawaran.png" width="200" alt="logo">
                </div>
                <div align="center">
                    <p align="center">
                        <br><br>
                        <font size="4">PEMERINTAHAN KABUPATEN PESAWARAN</font><br>
                        <font size="6">DINAS LINGKUNGAN HIDUP</font><br>
                        {{$kontak->alamat}}
                    </p>
                </div>

            </div>
        </div>
    </div>
@endsection
