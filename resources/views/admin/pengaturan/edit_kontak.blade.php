@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Pengaturan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Kontak</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($kontak, ['route' => ['admin.pengaturan.kontak.update', $kontak],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_kontak', ['model' => $kontak])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection