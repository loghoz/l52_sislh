@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Regulasi
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $regulasi->meta_key }}</h3>
                    </div>
                    <div class="box-body">
                        <object style="width: 100%; height: 500px;" type="application/pdf" data="/itlabil/file/{{$regulasi->meta_value}}?#zoom=85&scrollbar=0&toolbar=0&navpanes=0">
                            <p>File PDF tidak ditemukan.</p>
                        </object>
                    </div>
                    <div class="box-body" align="right">
                        <a href="{{ url('admin/regulasi') }}" class="btn btn-primary">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection