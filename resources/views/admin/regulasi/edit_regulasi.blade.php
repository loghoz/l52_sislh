@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Regulasi
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah {{$regulasi->meta_key}}</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($regulasi, ['route' => ['admin.regulasi.update', $regulasi],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_regulasi', ['model' => $regulasi])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
