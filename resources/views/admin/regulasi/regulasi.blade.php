@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Regulasi
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Regulasi</h3>
                    </div>
                    <div class="box-body" style="overflow-x:auto;">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Regulasi</td>
                                <td>Isi</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($regulasi as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->meta_key }}</td>
                                    <td>
                                        <a href="{{ route('admin.regulasi.show', $item->id) }}" class="btn btn-primary">Lihat</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.regulasi.edit', $item->id) }}" class="btn btn-success">Ubah</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
