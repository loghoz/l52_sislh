@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Berita
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Berita</h3>
                    </div>
                    <div class="box-body" style="overflow-x:auto;">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Judul Berita</td>
                                <td>Publish</td>
                                <td colspan="3" align="center">Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($berita as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->judul }}</td>
                                    <td>
                                        @if($item->publish=='T')
                                            <a href="{{ asset('admin/berita/publish') }}/{{$item->id}}" class="btn btn-primary">Aktif</a>
                                        @else
                                            <a href="{{ asset('admin/berita/publish') }}/{{$item->id}}" class="btn btn-default">Tidak Aktif</a>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <a href="{{ route('admin.berita.show', $item->id) }}" class="btn btn-primary">Lihat</a>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::model($item, ['route' => ['admin.berita.destroy', $item], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                                  {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                                {!! Form::close()!!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $berita->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
