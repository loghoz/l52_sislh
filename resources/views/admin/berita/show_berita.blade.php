@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Berita
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $berita->judul }}</h3>
                    </div>
                    <div class="box-body">
                        @if($berita->photo==="-")
                        @else
                            <center><img src="../../itlabil/images/berita/{{ $berita->photo }}" width="600" class="img-thumbnail" alt=""></center><br>
                        @endif
                        {!! $berita->isi !!}
                    </div>
                    <div class="box-body" align="right">
                        <a href="{{ url('admin/berita') }}" class="btn btn-primary">Kembali</a>
                        <a href="{{ route('admin.berita.edit', $berita->id) }}" class="btn btn-success">Ubah</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection