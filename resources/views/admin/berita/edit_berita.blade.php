@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Berita
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Berita</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($berita, ['route' => ['admin.berita.update', $berita],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_berita', ['model' => $berita])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
