@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Edit Banner
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah {{$banner->judul}}</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($banner, ['route' => ['admin.banner.update', $banner],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_banner', ['model' => $banner])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
