@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Banner
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Banner</h3>
                    </div>
                    @foreach($banner as $item)
                        <div class="box-header with-border">
                            {{ $item->judul }}<br><br>
                            <center><img src="{{url('itlabil/images/default')}}/{{ $item->photo }}" width="800" class="img-thumbnail" alt=""></center>
                            <div class="box-body" align="center">
                                <a href="{{ route('admin.banner.edit', $item->id) }}" class="btn btn-success">Ubah</a>
                            </div>
                        </div>
                        
                    @endforeach
                </div>

            </div>
        </div>
    </section>
@endsection
