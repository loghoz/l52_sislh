@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Layanan
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$data->meta_key}}</h3>
                    </div>
                    <div class="box-body">
                        {!! $data->meta_value !!}
                    </div>
                    <div class="box-body" align="right">
                        <a href="{{ route('admin.layanan.index') }}" class="btn btn-success">Kembali</a>
                        <a href="{{ route('admin.layanan.edit', $data->id) }}" class="btn btn-primary">Ubah</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
