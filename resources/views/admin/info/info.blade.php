@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Informasi
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <!-- Akun -->
            <div class="col-md-12">
                <div class="box box-primary">

                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.info.store', 'class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_info')
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="box box-primary">

                    <div class="box-body" style="overflow-x:auto;">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="150px">Judul</th>
                                <th>Isi</th>
                                <th colspan="2">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($info as $item)
                                <tr>
                                    <td><b>{{ $item->meta_key }}</b></td>
                                    <td>{!! str_limit($item->meta_value, 100) !!}</td>
                                    <td align="center">
                                        <a href="{{ route('admin.info.show', $item->id) }}" class="btn btn-success">Lihat</a>
                                    </td>
                                    <td align="center">
                                        <a href="{{ route('admin.info.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
