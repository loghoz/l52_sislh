@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Info
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$info->meta_key}}</h3>
                    </div>
                    <div class="box-body">
                        {!! $info->meta_value !!}
                    </div>
                    <div class="box-body" align="right">
                        <a href="{{ route('admin.info.index') }}" class="btn btn-success">Kembali</a>
                        <a href="{{ route('admin.info.edit', $info->id) }}" class="btn btn-primary">Ubah</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
