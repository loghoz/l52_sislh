@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Pengaturan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <!-- Akun -->
            <div class="col-md-6">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Akun Admin</h3>
                    </div>

                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td align="center">Email</td>
                                <td align="center">Password</td>
                                <td colspan="2" align="center">Aksi</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($user as $item)
                                <tr>
                                    <td>{{ $item->email }}</td>
                                    <td align="center">*****</td>
                                    <td align="center">
                                        <a href="{{ route('admin.pengaturan.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Kontak -->
            <div class="col-md-6">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Kontak</h3>
                    </div>

                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Telp</th>
                                <td>{{ $kontak->telp }}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{ $kontak->email }}</td>
                            </tr>
                            <tr>
                                <th>Instagram</th>
                                <td>{{ $kontak->instagram }}</td>
                            </tr>
                            <tr>
                                <th>Facebook</th>
                                <td>{{ $kontak->facebook }}</td>
                            </tr>
                            <tr>
                                <th>Twitter</th>
                                <td>{{ $kontak->twitter }}</td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>{{ $kontak->alamat }}</td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <a href="{{ route('admin.pengaturan.kontak.edit', $kontak->id) }}" class="btn btn-primary">Ubah</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
