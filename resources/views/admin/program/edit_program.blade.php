@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Program & Kebijakan
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Program & Kebijakan</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($data, ['route' => ['admin.programkebijakan.update', $data],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_info', ['model' => $data])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
