@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Album
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Album</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.gallery.store', 'class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_album')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="col-md-12">
                    @foreach($album as $item)
                        <div class="col-md-3">
                            <center>
                                @php($img='')
                                @foreach($item->gallery as $foto)
                                    @php($img = $foto->photo)
                                @endforeach
                                <a href="{{ route('admin.gallery.show', $item->id) }}">
                                    @if($img==='')
                                        <img src="../itlabil/admin/dist/img/images.png" alt="{{ $item->photo }}" class="img-thumbnail"><br>
                                    @else
                                        <img src="../itlabil/images/gallery/{{ $img }}" alt="{{ $item->photo }}" class="img-thumbnail" style="max-height:210px;"><br>
                                    @endif
                                </a>
                                
                                <h4>{{ $item->album }}</h3>
                                <!-- <div class="col-md-12" style="margin-top:5px;">
                                    <div class="col-md-4">
                                        <a href="{{ route('admin.gallery.show', $item->id) }}" class="btn btn-primary">Lihat</a>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="{{ route('admin.gallery.edit', $item->id) }}" class="btn btn-success">Edit</a>
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::model($item, ['route' => ['admin.gallery.destroy', $item], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                            {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                        {!! Form::close()!!}
                                    </div>
                                </div> -->
                                
                                
                            </center><br><br>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-12" align="center">
                    {{ $album->appends(compact('page'))->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection
