@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Album
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Album</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($album, ['route' => ['admin.gallery.update', $album],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_album', ['model' => $album])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
