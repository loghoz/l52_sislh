@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Album : {{$album->album}}
        </h1>
    </section>

    <div class="row" style="padding-top:10px;">
        <div class="col-md-12">
            <div class="col-md-2">
                {!! Form::model($album->id, ['route' => ['admin.gallery.destroy', $album->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                    {!! Form::submit('Hapus Album', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                {!! Form::close()!!}
            </div>
            <div class="col-md-2">
                <a href="{{ route('admin.gallery.edit', $album->id) }}" class="btn btn-success">Edit Album</a>
            </div>
            <div class="col-md-8"></div>
        </div>
    </div>
        
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Gambar</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.foto.store', 'class'=>'form-horizontal','files'=>true,'enctype'=>'multipart/form-data'])!!}
                            @include('form._admin_gallery')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="col-md-12">
                    @foreach($foto as $item)
                        <div class="col-md-3">
                            <center>
                                <img src="../../itlabil/images/gallery/{{ $item->photo }}" alt="{{ $item->photo }}" class="img-thumbnail">
                                <div style="margin-top:5px;">
                                    {!! Form::model($item, ['route' => ['admin.foto.destroy', $item], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                        {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                    {!! Form::close()!!}
                                </div>
                                
                            </center><br><br>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-12" align="center">
                    {{ $foto->appends(compact('page'))->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection