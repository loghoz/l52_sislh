@extends('layouts.beranda')

@section('content')

    <!-- MOTO -->
    <section id="layanan" data-stellar-background-ratio="1">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12">
                    <div class="layanan-title">
                        <h2 class="wow fadeInUp" data-wow-delay="0.1s">UKL-UPL / DPLH</h2>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 col-sm-12">
                    <div class="col-md-12 col-sm-12" align="center">
                        <img src="../../itlabil/images/default/ukl.png" width="1000px" class="img-responsive" alt=""><br>
                    </div>
                    @foreach($meta as $item)
                        @if($item->meta_key=='UKL')
                            {!! $item->meta_value !!}
                        @endif
                    @endforeach
                </div>
                
            </div>
        </div>
    </section>

@endsection
