@extends('layouts.beranda')

@section('content')

    <!-- SASARAN -->
    <section id="layanan" data-stellar-background-ratio="1">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12">
                    <div class="layanan-title">
                        <h2 class="wow fadeInUp" data-wow-delay="0.1s">SASARAN</h2>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 col-sm-12">
                    <center>
                        @foreach($meta as $item)
                            @if($item->meta_key=='Profil - Sasaran')
                                {!! $item->meta_value !!}
                            @endif
                        @endforeach
                    </center>
                </div>
                
            </div>
        </div>
    </section>

@endsection
