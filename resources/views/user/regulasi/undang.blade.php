@extends('layouts.beranda')

@section('content')

    <!-- MOTO -->
    <section id="layanan" data-stellar-background-ratio="1">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12">
                    <div class="layanan-title">
                        <h2 class="wow fadeInUp" data-wow-delay="0.1s">UNDANG - UNDANG</h2>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 col-sm-12">
                    <!-- <p align="center">
                    1. Undang-Undang Nomor 32 Tahun 2009 tentang Perlindungan dan Pengelolaan Lingkungan Hidup<br>
                    2. Undang-Undang Nomor 18 Tahun 2008 tentang Pengelolaan Sampah
                    </p> -->

                    @foreach($regulasi as $item)
                        <object class="view-pdf" type="application/pdf" data="/itlabil/file/{{ $item->meta_value }}?#zoom=85&scrollbar=0&toolbar=0&navpanes=0">
                            <p>File PDF tidak ditemukan.</p>
                        </object>
                    @endforeach
                </div>
                
            </div>
        </div>
    </section>

@endsection
