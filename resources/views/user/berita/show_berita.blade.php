@extends('layouts.beranda')

@section('content')

    <section id="layanan" data-stellar-background-ratio="2.5">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <div class="col-md-12 col-sm-12">
                        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                            <font size="6"><b>{{ $berita->judul }}</b></font><br><font color="#4cae4c">Tanggal : {{ $berita->created_at->format('d M Y') }}</font>
                        </div>
                        @if($berita->photo==="-")
                        @else
                            <center><img src="../itlabil/images/berita/{{ $berita->photo }}" class="img-berita-post img-thumbnail" alt=""></center><br>
                        @endif
                        {!! $berita->isi !!}
                        <p class="share">Share : 
                            <a href="https://www.facebook.com/sharer/sharer.php?u=http://dinaslh.pesawarankab.go.id/berita/{{$berita->id}}" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/share?url=http://dinaslh.pesawarankab.go.id/berita/{{$berita->id}}&text=""via=erwindianto" target="_blank"><i class="fa fa-twitter"></i></a>
                            <!-- <a href="http://localhost/berita/{{$berita->id}}" target="_blank"><i class="fa fa-instagram"></i></a> -->
                        </p>
                        <br><br>
                    </div>
                    <div class="col-md-12 col-sm-12 komentar">
                        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                        <br><font size="6"><b>Komentar</b></font>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            {!! Form::open(['route' => 'berita.store', 'class'=>'form-horizontal'])!!}
                                @include('form._admin_komentar')
                            {!! Form::close() !!}
                        </div>
                        
                        <div class="col-md-12"><hr>
                            @foreach($komentar as $koment)
                            <div class="col-md-6">
                                Nama : {{ $koment->nama }}
                            </div>
                            <div class="col-md-6" align="right">
                                <font color="#4cae4c">Tanggal {{ $koment->created_at->format('d M Y') }}</font><br>
                            </div>
                            <div class="col-md-12">
                                Email : {{ $koment->email }}<br><br>
                                <font color="#4cae4c">Komentar </font>:<br>{{ $koment->komentar}}
                                @if($koment->balasan!='')
                                    <br><br>
                                    <font color="#337ab7">Admin </font>:<br>{{ $koment->balasan}}
                                @endif
                            <hr>
                            </div>
                            @endforeach
                            {{ $komentar->appends(compact('page'))->links() }}
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-3">
                    <div class="col-md-12 col-sm-12">
                        <!-- SECTION TITLE -->
                        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                            <h4>Berita Lain</h4>
                        </div>
                    </div>
                    @foreach($beritalain as $populer)
                        <div class="col-md-12">
                            <div class="news-thumb wow fadeInUp" data-wow-delay="0.4s">
                                <div class="news-info">
                                    <span>{{ $populer->created_at->format('d M Y') }}</span>
                                    <b><a href="{{ route('berita.show', $populer->id) }}" title="{{ $populer->judul }}">{{ str_limit($populer->judul, 30) }}</a></b>
                                    <p>{!! str_limit($populer->isi, 70) !!}</p>
                                </div>
                            </div><br>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection