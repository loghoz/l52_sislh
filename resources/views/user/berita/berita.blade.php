@extends('layouts.beranda')

@section('content')

    <section id="layanan" data-stellar-background-ratio="2.5">
            <div class="container">
                <div class="row">
    
                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                                    <h2>Berita Terbaru</h2>
                            </div>
                        </div>
                        
                        @foreach($berita as $item)
                        <div class="col-md-4">
                            <div class="news-thumb wow fadeInUp" data-wow-delay="0.4s">
                                <a href="{{ route('berita.show', $item->id) }}">
                                    @if( $item->photo == "-")
                                        <img class="img-berita img-responsive" src="itlabil/images/default/default-berita.png" alt="">
                                    @else	
                                        <img src="itlabil/images/berita/{{ $item->photo }}" class="img-berita img-responsive" alt="">	
                                    @endif		  
                                </a>
                                <div class="news-info">
                                    <span>{{ $item->created_at->format('d M Y') }}</span>
                                    <h3><a href="{{ route('berita.show', $item->id) }}" title="{{ $item->judul }}">{{ str_limit($item->judul, 30) }}</a></h3>
                                    <p>{!! str_limit($item->isi, 70) !!}</p>
								</div>
                            </div><br>
                        </div>

                        @endforeach
                        
                        <div class="col-md-12" align="center">
                            {{ $berita->appends(compact('page'))->links() }}
                        </div>
                    </div>
                </div>
        </section>


@endsection
