@extends('layouts.beranda')

@section('content')

    <section id="layanan" data-stellar-background-ratio="2.5">
        <div class="container">
            <div class="tz-gallery">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- SECTION TITLE -->
                        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                            <h3>Album {{$album->album}}</h3>
                        </div>
                    </div>
                        
                    <div class="col-md-12">
                        @foreach($foto as $item)
                            <div class="col-md-3">
                                <a class="lightbox" href="../../itlabil/images/gallery/{{ $item->photo }}">
                                    <img src="../../itlabil/images/gallery/{{ $item->photo }}" alt="{{ $item->photo }}" class="img-thumbnail">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-8" align="center">
                        {{ $foto->appends(compact('page'))->links() }}
                    </div>
                    <div class="col-md-4" align="center">
                        <a href="/gallery" class="btn btn-primary">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
