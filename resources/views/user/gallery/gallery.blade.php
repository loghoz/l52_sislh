@extends('layouts.beranda')

@section('content')

    <section id="layanan" data-stellar-background-ratio="1">
        <div class="container">

            <div class="tz-gallery">

                <div class="row">
                    @foreach($album as $item)
                        <div class="col-md-3">
                            <center>
                                @php($img='')
                                @foreach($item->gallery as $foto)
                                    @php($img = $foto->photo)
                                @endforeach
                                <a href="{{ route('gallery.show', $item->id) }}">
                                    @if($img==='')
                                        <img src="../itlabil/admin/dist/img/images.png" alt="{{ $item->photo }}" class="img-thumbnail"><br>
                                    @else
                                        <img src="../itlabil/images/gallery/{{ $img }}" alt="{{ $item->photo }}" class="img-thumbnail" style="max-height:210px;"><br>
                                    @endif
                                </a>
                                
                                <h5>{{ $item->album }}</h5>
                            </center><br><br>
                        </div>
                    @endforeach
                </div>
                
                <div class="col-md-12" align="center">
                    {{ $album->appends(compact('page'))->links() }}
                </div>
            </div>
        </div>
    </section>

@endsection
