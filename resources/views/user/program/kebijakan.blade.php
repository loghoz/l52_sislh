@extends('layouts.beranda')

@section('content')

    <!-- MOTO -->
    <section id="layanan" data-stellar-background-ratio="1">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12">
                    <div class="layanan-title">
                        <h2 class="wow fadeInUp" data-wow-delay="0.1s">KEBIJAKAN</h2>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 col-sm-12">
                    @foreach($meta as $item)
                        @if($item->meta_key=='PK - Kebijakan')
                            {!! $item->meta_value !!}
                        @endif
                    @endforeach
                </div>
                
            </div>
        </div>
    </section>

@endsection
