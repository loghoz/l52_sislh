<div class="col-sm-12">

    {{--  PHOTO  --}}
    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
        {!! Form::label('photo', 'Foto', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-8">
            <!-- {!! Form::file('photo') !!} -->
            <input type="file" name="photo[]" multiple>

        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <small class="text-danger">{{ $errors->first('photo') }}</small>
            <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB <br>dan Resolusi Gambar Square ex: 800 x 600 pixel</p>
        </div>
    </div>
    <input type="hidden" id="album_id" name="album_id" value="{{$album->id}}">
    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
