<div class="col-sm-12">
    {{--  PHOTO  --}}
    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
        {!! Form::label('photo', 'Foto', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-8">
            {!! Form::file('photo') !!}
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <small class="text-danger">{{ $errors->first('photo') }}</small>
            <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB <br>
                Resolusi Gambar Square ex: 1920 x 1280 pixel <br>
                Extensi Gambar Harus .jpg</p>
        </div>
    </div>

    <div class="col-md-12">
        <br><p>Foto Sebelumnya:</p>
        <div class="thumbnail">
            <img src="{{ url('itlabil/images/default/' . $banner->photo) }}" width="600px" class="img-rounded">
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
