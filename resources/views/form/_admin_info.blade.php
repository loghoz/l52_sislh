<div class="col-sm-12">

    <div class="form-group{{ $errors->has('meta_key') ? ' has-error' : '' }}">
        {!! Form::label('meta_key', 'Judul', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-6">
          {!! Form::text('meta_key', null, ['class' => 'form-control','placeholder'=>'Judul']) !!}
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
          <small class="text-danger">{{ $errors->first('meta_key') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('meta_value') ? ' has-error' : '' }}">
        {!! Form::label('meta_value', 'Isi', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-10">
            {!! Form::textarea('meta_value', null, ['class' => 'textarea','placeholder'=>'Isi','style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
        <small class="text-danger">{{ $errors->first('meta_value') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>

</div>