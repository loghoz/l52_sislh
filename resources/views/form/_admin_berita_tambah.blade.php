<div class="col-sm-12">
    <div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
        {!! Form::label('judul', 'Judul Berita', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-8">
          {!! Form::text('judul', null, ['class' => 'form-control','placeholder'=>'Judul Berita']) !!}
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('judul') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('isi') ? ' has-error' : '' }}">
        {!! Form::label('isi', 'Berita', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-8">
            <div class="box-body pad">
                {!! Form::textarea('isi', null, ['class' => 'textarea','placeholder'=>'Ketik Berita','style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('isi') }}</small>
        </div>
    </div>

    {{--  PHOTO  --}}
    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
        {!! Form::label('photo', 'Foto', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-10">
            {!! Form::file('photo') !!}
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
            <small class="text-danger">{{ $errors->first('photo') }}</small>
            <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB <br>dan Resolusi Gambar Square ex: 800 x 600</p>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
