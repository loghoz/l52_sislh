<div class="col-sm-12">

    <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
        {!! Form::label('telp', 'No Telp', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('telp', null, ['class' => 'form-control','placeholder'=>'Nomor Telepon']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('telp') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', 'Email', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'Email']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('email') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('instagram') ? ' has-error' : '' }}">
        {!! Form::label('instagram', 'Instagram', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
        {!! Form::text('instagram', null, ['class' => 'form-control','placeholder'=>'Instagram']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
        <small class="text-danger">{{ $errors->first('instagram') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
        {!! Form::label('facebook', 'Facebook', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
        {!! Form::text('facebook', null, ['class' => 'form-control','placeholder'=>'Facebook']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
        <small class="text-danger">{{ $errors->first('facebook') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
        {!! Form::label('alamat', 'Alamat', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
        {!! Form::textarea('alamat', null, ['class' => 'form-control','placeholder'=>'Alamat']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
        <small class="text-danger">{{ $errors->first('alamat') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>

</div>