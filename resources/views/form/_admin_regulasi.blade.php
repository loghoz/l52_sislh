<div class="col-sm-12">
    {{--  meta_value  --}}
    <div class="form-group{{ $errors->has('meta_value') ? ' has-error' : '' }}">
        {!! Form::label('meta_value', 'File', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-10">
            {!! Form::file('meta_value') !!}
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
            <small class="text-danger">{{ $errors->first('meta_value') }}</small>
            <p>Pastikan Ukuran File Tidak Lebih dari 2 MB</p>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
