<div class="col-sm-12">

    {{--  PHOTO  --}}
    <div class="form-group{{ $errors->has('album') ? ' has-error' : '' }}">
        {!! Form::label('album', 'Album Foto', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-8">
          {!! Form::text('album', null, ['class' => 'form-control','placeholder'=>'Album Foto']) !!}
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('album') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
