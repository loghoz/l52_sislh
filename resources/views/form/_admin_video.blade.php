<div class="col-sm-12">
    <div class="form-group{{ $errors->has('video') ? ' has-error' : '' }}">
        {!! Form::label('video', 'Link Video', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-8">
          {!! Form::text('video', null, ['class' => 'form-control','placeholder'=>'Link Video']) !!}
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('video') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
