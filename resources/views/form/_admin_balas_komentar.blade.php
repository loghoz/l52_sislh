<div class="col-sm-12">
    <div class="form-group{{ $errors->has('komentar') ? ' has-error' : '' }}">
        {!! Form::label('komentar', 'Komentar', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-8">
            <div class="box-body pad">
                {{$komentar->komentar}}
            </div>
        </div>
    </div>
    <div class="form-group{{ $errors->has('balasan') ? ' has-error' : '' }}">
        {!! Form::label('balasan', 'Balasan', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-8">
            <div class="box-body pad">
                {!! Form::textarea('balasan', null, ['placeholder'=>'Tulis Balasan','style' => 'width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('balasan') }}</small>
        </div>
    </div>
    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Kirim", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
