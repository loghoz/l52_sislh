<div class="col-sm-12">
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {!! Form::label('password', 'Password', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('password', null, ['class' => 'form-control','placeholder'=>'Kosongkan jika tidak merubah password']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('password') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
